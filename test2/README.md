# Test 2

### Task

Разработать приложение на Django для просмотров нескольких примеров highcharts.
Данные, передаваемые в графики должны быть сгенерированы «самописно».

Типы графиков:
line,
area

Должна быть простая страница (черно/белая) с двумя элементами:
1) комбобокс с типом графиков (line, area)
2) график

### How it works

###### Prepare

	git clone https://gitlab.com/vvkuznetsov/start-i.git
	cd start-i/test2

###### Run tests
	
	mkdir test2/static test2/static_root
	./manage.py collectstatic

	./manage.py runserver

###### Results

You can see results in [browser] (http://127.0.0.1:8000/)

* * *
There are test assignments from [Start-i] (http://www.start-i.ru/)
by Sulimov Alexandr <alexander.soulimov@start-i.ru>.